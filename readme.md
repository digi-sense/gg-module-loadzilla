# LoadZilla #

![icon](icon_128.png)

LOAD - MONITOR - LEARN

The lightweight ([binaries also for Raspberry Pi](./_build/linux_raspberry_3)) Open Source server Load Testing tool written in Go. 

With LoadZilla you can:

- **Load** test your web server
- **Monitor** your server status (is alive or is down?)
- **Learn** about your server performance

## Introduction ##

LoadZilla has been designed for testing Web Applications and monitor application performance or at least if an
application server is still working.

LoadZilla may be used to test performance both on static and dynamic resources, Web dynamic applications.

It can be used to simulate a heavy load on a server, group of servers, network or object to test its strength or to
analyze overall performance under different load types.

## LoadZilla Report ##

LoadZilla generates a report and email you every time a new report is ready.
The email report contains 2 attachments:

 - the [data.csv](./_docs/data.csv) file with the action log
 - the [report.htm](./_docs/report.html) report

Here is a sample report:

![report](./_docs/media/report-email.png)

## Usage ##

Typically, you should install LoadZilla on a server and configure it to run tests on other servers.

A single LoadZilla instance can test or monitor many servers depending on 
host server capacity and resources.

## Configuration ##

LoadZilla has typically two different type of settings:

 - System settings
 - Job (or Action) settings

### System Settings ###

System settings allow configuring server notification providers:

 - EMAIL: declare email smtp server
 - SMS: declare SMS gateway

Below a `settings.production.json` system settings file:

```json
{
  "sms": {
    "enabled": false,
    "auto-short-url": false,
    "providers": {
      "default": {
        "driver": "generic",
        "method": "GET",
        "endpoint": "https://api.smshosting.it/rest/api/smart/sms/send?authKey={{auth-key}}&authSecret={{auth-secret}}&text={{message}}&to={{to}}&from={{from}}",
        "params": {
          "auth-key": "xxxxx",
          "auth-secret": "xxxx",
          "message": "",
          "to": "",
          "from": "TEST-SRV"
        },
        "headers": {}
      }
    }
  },
  "email": {
    "enabled": true,
    "secure": true,
    "host": "pro.turbo-smtp.com",
    "port": 465,
    "from": "Gian Angelo Geminiani <angelo.geminiani@ggtechnologies.sm>",
    "auth": {
      "user": "xxxxxx",
      "pass": "xxxxxx"
    }
  }
}
```

Notification providers are used to send alerts and reports.

### Action Settings ###

Actions (or Jobs) are the core of LoadZilla.
LoadZilla support multiple simultaneous tasks in a multithreading 
environment.

Each thread is executed in a separate environment handled with a Goroutine 
(A goroutine is a lightweight thread managed by the Go runtime).
A goroutine is a lightweight thread. It costs a lot less than a real OS thread, and multiple goroutines may be 
multiplexed onto a single OS thread.

So, we can say that LoadZilla handle Actions as an _independent concurrent thread of control within 
the same address space_.

Actions (or Jobs) are directories under the master `jobs` directory.
Each LoadZilla job directory contains at least a `job.json` file.

Below a `job.json` sample file:

```json
{
  "uid": "default",
  "enabled": true,
  "schedule": {
    "start_at": "",
    "timeline": "second:2"
  },
  "threads": 1,
  "action": {
    "script": "",
    "method": "GET",
    "endpoint": "https://gianangelogeminianik.me",
    "header": {
      "APP_TOKEN": "juygefuigwheirufgiwurg"
    },
    "body": {},
    "notification": {
      "email": {
        "to": "email@domain.com",
        "subject": "ERROR: {{ .err }}",
        "message": "An error occurred checking '{{ .endpoint }}' endpoint:\n\t {{ .err }}"
      },
      "sms": {
        "to": "+393412345678",
        "message": "An error occurred checking '{{ .endpoint }}' endpoint: {{ .err }}"
      }
    }
  },
  "report": {
    "name": "",
    "auto-remove": true,
    "schedule": {
      "start_at": "",
      "timeline": "hour:1"
    },
    "email": {
      "to": "email@domain.com",
      "subject": "LoadZilla Report {{ .date }}"
    },
    "sms": {
      "to": "+39.....",
      "message": "LoadZilla Report: {{ .url }}"
    }
  }
}
```

### LoadZilla Directories Structure ###

![report](./_docs/media/directories.png)

LoadZilla start working a `WORKSPACE` directory.
The default workspace name is `_loadzilla`, but you can change it passing a parameter to executable.

LoadZilla directories are:
 - jobs: contains all jobs to execute
 - logging: contains all log files
 - reports: contains all output reports and data in CSV format

#### JOBS ####

The `./jobs` folder is the place tha contains all jobs for LoadZilla to execute.

For each job is required to create a specific directory with job name. For example, if your job name is "sample"
the folder will be `./jobs/sample`. The same will be if you want name a job "checkMyWebSite"
the folder will be `./jobs/checkMyWebSite`.

LoadZilla at start check `./jobs` folder and creates a thread for each job found.

![report](./_docs/media/dir_jobs.png)

#### REPORTS ####

The `./reports` folder is specular to `./jobs` and contains all job directories with related reports and data.

![report](./_docs/media/dir_reports.png)

#### LOGGING ####

The `./logging` folder contains all log files.

The main log file is `logging.log` and contains system errors or notification.

All other .log files are the job's logs. Each job log file name is the "uid" field value of job configuration file.

![report](./_docs/media/dir_logging.png)

## Supported Protocols ##

Jobs can perform actions sending data to a remote endpoint.

When a Job is executed there are 3 different steps that LoadZilla perform:
 1. `on_before`: Invoke an optional javascript program
 2. `network action`: open a client over a network protocol (http/s, ping, etc..) and send data
 3. `on_after`: Invoke an optional javascript program

Finally, the Job receive a response and send a notification to a user and a reporting system.

Network Actions are executed in different flavors:
 - HTTP: send GET, POST, PUT or DELETE request with a custom body and headers.
 - PING: send a ping using the host Operating System `ping` command

### HTTP ###

Below is a sample http Job configuration.

```json
{
  "uid": "default",
  "enabled": true,
  "schedule": {
    "start_at": "",
    "timeline": "second:2"
  },
  "threads": 1,
  "action": {
    "script": {
      "on_before": "",
      "on_after": ""
    },
    "method": "GET",
    "endpoint": "https://gianangelogeminiani.me?timestamp=<var>rnd|numeric|15</var>",
    "header": {
      "APP_TOKEN": "juygefuigwheirufgiwurg"
    },
    "body": {},
    "notification": {
      "email": {
        "to": "angelo.....",
        "subject": "ERROR: {{ .err }}",
        "message": "An error occurred checking '{{ .endpoint }}' endpoint:\n\t {{ .err }}",
        "attachments": ["./logging/logging.log", "./logging/default.log"]
      },
      "sms": {
        "to": "+39......",
        "message": "An error occurred checking '{{ .endpoint }}' endpoint: {{ .err }}"
      }
    }
  },
  "report": {
    "name": "",
    "auto-remove": true,
    "schedule": {
      "start_at": "",
      "timeline": "hour:1"
    },
    "email": {
      "to": "angelo.....",
      "subject": "LoadZilla Report {{ .date }}"
    },
    "sms": {
      "to": "+39.....",
      "message": "LoadZilla Report: {{ .url }}"
    }
  }
}
```

### PING ###

Below is a sample ping Job configuration.

```json
{
  "uid": "default",
  "enabled": true,
  "schedule": {
    "start_at": "",
    "timeline": "second:1"
  },
  "threads": 1,
  "action": {
    "script": {
      "on_before": "",
      "on_after": ""
    },
    "method": "PING",
    "endpoint": "ping://www.gianangelogeminiani.me",
    "header": {},
    "body": {},
    "notification": {
      "email": {
        "to": "angelo.....",
        "subject": "ERROR: {{ .err }}",
        "message": "An error occurred checking '{{ .endpoint }}' endpoint:\n\t {{ .err }}",
        "attachments": ["./logging/logging.log", "./logging/default.log"]
      },
      "sms": {
        "to": "+39......",
        "message": "An error occurred checking '{{ .endpoint }}' endpoint: {{ .err }}"
      }
    }
  },
  "report": {
    "name": "",
    "auto-remove": true,
    "schedule": {
      "start_at": "",
      "timeline": "hour:1"
    },
    "email": {
      "to": "angelo.....",
      "subject": "LoadZilla Report {{ .date }}"
    },
    "sms": {
      "to": "+39.....",
      "message": "LoadZilla Report: {{ .url }}"
    }
  }
}
```

Ping has no `body` and no `header` attributes. 
To declare a ping the only you  need is write a prefix `ping://` before IP or URL to ping.

For example: `ping://www.google.com` send a ping to google.

## Javascript Engine ##

LoadZilla embeds a javascript runtime tha you can use to execute custom activities.

Javascript programs are invoked in two different ways:

1. `on_before`: Before Job network action is executed
2. `on_after`: After Job network action is executed

For example below is a javascript program returning an object used later in other 
javascript program (the variable $ctx give access to environment context) or in network request
(using formula expression engine `<var>ctx|var1</var>`).

```javascript
(function(){
  var var1 = $ctx["var1"]||"";
    console.log("logme.js is working fine!");
    console.log("var1=", var1);
    return {
        "var1":"This message is written in js engine"
    }
})();
```

## Formula Expression Engine ##

LoadZilla embeds a ["formula expression"](https://bitbucket.org/digi-sense/gg-core/src/master/gg_fnvars/readme.md) engine that allow you to add parametric 
values to network requests.

For example:

`https://gianangelogeminianiq.me?timestamp=<var>rnd|numeric|15</var>`

The formula `<var>rnd|numeric|15</var>` add to `https://gianangelogeminianiq.me?timestamp=` a 15 digits random string.

Once solved the formula will return something like:
`https://gianangelogeminianiq.me?timestamp=132456534753678`

## Binaries ##

LoadZilla can be used as a Go module or as a stand-alone program.

[Click here](https://bitbucket.org/digi-sense/gg-module-loadzilla/src/master/_build/) for binaries.

### Executable Commands and Parameters ###

Sample command to launch LoadZilla: `loadzilla run -dir_work=./_loadzilla`

**LoadZilla Commands:**

- run : Execute LoadZilla runtime

More commands are coming...

**LoadZilla supports some parameters at launch:**

 - `-dir_work` : Workspace folder.


### Go Module ###

Sources are versioned using git tags:

```
git tag v0.1.8
git push origin v0.1.8
```

To use just call:

```
go get bitbucket.org/digi-sense/gg-module-loadzilla@latest
```

