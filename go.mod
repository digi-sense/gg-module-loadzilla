module bitbucket.org/digi-sense/gg-module-loadzilla

go 1.16

require (
	bitbucket.org/digi-sense/gg-core v0.1.24 // indirect
	bitbucket.org/digi-sense/gg-core-x v0.1.24 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
)
