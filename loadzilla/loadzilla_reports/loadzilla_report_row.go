package loadzilla_reports

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"fmt"
	"time"
)

type LoadZillaReportRow struct {
	InstanceIndex  int    `json:"instance-index"` // index counter of multi-thread job
	Timestamp      string `json:"timestamp"`
	DateTime       string `json:"date-time"`
	Level          string `json:"level"` // info, error
	SmsMessage     string `json:"sms-message"`
	EmailMessage   string `json:"email-message"`
	ElapsedMs      int    `json:"elapsed-ms"`
	Name           string `json:"name"`
	Uid            string `json:"uid"`
	Source         string `json:"source"`
	ResponseLength int    `json:"response-length"`
	Error          string `json:"error"`
}

func NewLoadZillaReportRowFromMap(m map[string]string) *LoadZillaReportRow {
	return &LoadZillaReportRow{
		InstanceIndex:  gg.Convert.ToInt(gg.Reflect.Get(m, "instance-index")),
		Timestamp:      gg.Reflect.GetString(m, "timestamp"),
		DateTime:       gg.Reflect.GetString(m, "date-time"),
		Level:          gg.Reflect.GetString(m, "level"),
		SmsMessage:     gg.Reflect.GetString(m, "sms-message"),
		EmailMessage:   gg.Reflect.GetString(m, "email-message"),
		ElapsedMs:      gg.Convert.ToInt(gg.Reflect.Get(m, "elapsed-ms")),
		Name:           gg.Reflect.GetString(m, "name"),
		Uid:            gg.Reflect.GetString(m, "uid"),
		Source:         gg.Reflect.GetString(m, "source"),
		ResponseLength: gg.Convert.ToInt(gg.Reflect.Get(m, "response-length")),
		Error:          gg.Reflect.GetString(m, "error"),
	}
}

func NewLoadZillaReportRow(instanceIndex int, name, uid, source, smsMessage, emailMessage string, err error, elapsed int, respLen int) *LoadZillaReportRow {
	instance := new(LoadZillaReportRow)
	instance.InstanceIndex = instanceIndex
	instance.Timestamp = fmt.Sprintf("%d", time.Now().Unix())
	instance.DateTime = time.Now().Format(loadzilla_commons.DateLayout)
	instance.Name = name
	instance.Uid = uid
	instance.Source = source
	instance.SmsMessage = smsMessage
	instance.EmailMessage = emailMessage
	if nil != err {
		instance.Error = err.Error()
		instance.Level = "error"
	} else {
		instance.Level = "info"
	}
	instance.ElapsedMs = elapsed
	instance.ResponseLength = respLen

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaReportRow) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LoadZillaReportRow) Map() map[string]interface{} {
	return gg.Convert.ToMap(gg.JSON.Stringify(instance))
}

func (instance *LoadZillaReportRow) MapOfString() map[string]string {
	return gg.Convert.ToMapOfString(gg.JSON.Stringify(instance))
}
