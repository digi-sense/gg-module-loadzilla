package loadzilla_reports

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"fmt"
	"time"
)

const projectSourceUrl = "https://bitbucket.org/digi-sense/gg-module-loadzilla/src/master/readme.md"
const projectSiteUrl = "https://bitbucket.org/digi-sense/gg-module-loadzilla/src/master/readme.md"
const projectLogo = "https://bitbucket.org/digi-sense/gg-module-loadzilla/raw/7460ab53a27e583bb8d48cf5b6994e0751b2c152/icon_128.png"

type LoadZillaReportSummary struct {
	root            string
	summaryFileName string

	ProjectLogo            string    `json:"project_logo"`
	ProjectUrl             string    `json:"project_source_url"`
	SiteUrl                string    `json:"project_site_url"`
	HostID                 string    `json:"host_id"`
	HostOS                 string    `json:"host_os"`
	HostCPU                int       `json:"host_cpu"`
	HostCore               string    `json:"host_core"`
	HostMemoryUsage        string    `json:"host_memory_usage"`
	Hostname               string    `json:"host_name"`
	HostKernel             string    `json:"host_kernel"`
	HostPlatform           string    `json:"host_platform"`
	Name                   string    `json:"name"`
	Length                 int       `json:"length"`
	StartDate              time.Time `json:"start_date"`
	EndDate                time.Time `json:"end_date"`
	StartDateFmt           string    `json:"start_date_fmt"`
	EndDateFmt             string    `json:"end_date_fmt"`
	Date                   string    `json:"date"`       // report date
	DurationTime           string    `json:"range_time"` // monitoring time
	ElapsedMsMin           int       `json:"elapsed_ms_min"`
	ElapsedMsMax           int       `json:"elapsed_ms_max"`
	ElapsedMsAvg           int       `json:"elapsed_ms_avg"`
	ElapsedMsErrMin        int       `json:"elapsed_ms_err_min"`
	ElapsedMsErrMax        int       `json:"elapsed_ms_err_max"`
	ElapsedMsErrAvg        int       `json:"elapsed_ms_err_avg"`
	ErrorsCount            int       `json:"errors_count"`
	Errors                 []string  `json:"errors"`
	ErrorsFmt              string    `json:"errors_fmt"`
	ErrorPerc              float32   `json:"errors_perc"`
	ErrorPercFmt           string    `json:"errors_perc_fmt"`
	EndpointsCount         int       `json:"endpoints_count"`
	Endpoints              []string  `json:"endpoints"`
	EndpointsFmt           string    `json:"endpoints_fmt"`
	SettingsThreads        int       `json:"settings_threads"`
	SettingsTimeline       string    `json:"settings_timeline"`
	SettingsReportTimeline string    `json:"settings_report_timeline"`

	Total *LoadZillaReportSummaryTotals `json:"totals"`
}

type LoadZillaReportSummaryTotals struct {
	Check                 int     `json:"check_count"`
	CheckWithErrors       int     `json:"check_with_errors"`
	CheckWithNoErrors     int     `json:"check_with_no_errors"`
	Errors                int     `json:"errors"`
	Length                int     `json:"length"`
	QualityServicePerc    float32 `json:"quality_service_perc"`
	QualityServicePercFmt string  `json:"quality_service_perc_fmt"`
}

func NewLoadZillaReportSummary(root string, settings *loadzilla_commons.LoadZillaJobSettings, name string, rows []*LoadZillaReportRow) *LoadZillaReportSummary {
	report := new(LoadZillaReportSummary)
	report.root = root
	report.summaryFileName = gg.Paths.Concat(root, name, "summary.json")

	report.Name = name
	report.ProjectUrl = projectSourceUrl
	report.SiteUrl = projectSiteUrl
	report.ProjectLogo = projectLogo
	report.HostID, _ = gg.Sys.ID()
	report.HostOS = gg.Sys.GetOS()
	info := gg.Sys.GetInfo()
	if nil != info {
		report.HostCPU = info.CPUs
		report.HostCore = info.Core // version
		report.HostMemoryUsage = info.MemoryUsage
		report.Hostname = info.Hostname
		report.HostKernel = info.Kernel     // i.e. Darwin
		report.HostPlatform = info.Platform // x86
	}
	report.Date = time.Now().Format(loadzilla_commons.DateLayout)
	report.Errors = make([]string, 0)
	report.Endpoints = make([]string, 0)
	report.StartDate = time.Now()
	report.EndDate = time.Now()

	// settings
	report.SettingsThreads = settings.Threads
	report.SettingsTimeline = settings.Schedule.Timeline
	report.SettingsReportTimeline = settings.Report.Schedule.Timeline

	if len(rows) > 0 {
		report.Length = len(rows)
		totElapsed := 0
		totElapsedErr := 0
		for i, row := range rows {
			// elapsed & errors
			if len(row.Error) == 0 {
				totElapsed += row.ElapsedMs
				if report.ElapsedMsMin == 0 || row.ElapsedMs < report.ElapsedMsMin {
					report.ElapsedMsMin = row.ElapsedMs
				}
				if report.ElapsedMsMax < row.ElapsedMs {
					report.ElapsedMsMax = row.ElapsedMs
				}
			} else {
				report.ErrorsCount++
				report.Errors = gg.Arrays.AppendUnique(report.Errors, row.Error).([]string)
				totElapsedErr += row.ElapsedMs
				if report.ElapsedMsErrMin == 0 || row.ElapsedMs < report.ElapsedMsErrMin {
					report.ElapsedMsErrMin = row.ElapsedMs
				}
				if report.ElapsedMsErrMax < row.ElapsedMs {
					report.ElapsedMsErrMax = row.ElapsedMs
				}
			}

			// time and monitoring duration
			if i == 0 {
				report.StartDate, _ = time.Parse(loadzilla_commons.DateLayout, row.DateTime)
			}
			if i >= len(rows)-1 {
				report.EndDate, _ = time.Parse(loadzilla_commons.DateLayout, time.Now().Format(loadzilla_commons.DateLayout))
			}

			// endpoints
			if len(row.Source) > 0 {
				report.Endpoints = gg.Arrays.AppendUnique(report.Endpoints, row.Source).([]string)
				report.EndpointsCount++
			}
		} // end-for

		report.ElapsedMsAvg = totElapsed / report.Length
		report.ElapsedMsErrAvg = totElapsedErr / report.Length
		report.ErrorPerc = float32(report.ErrorsCount) / float32(report.Length)
		report.ErrorPercFmt = fmt.Sprintf("%.2f%%", report.ErrorPerc*100)
		for i, e := range report.Errors {
			if i > 0 {
				report.ErrorsFmt += ", "
			}
			report.ErrorsFmt += "\"" + e + "\""
		}
		for i, e := range report.Endpoints {
			if i > 0 {
				report.EndpointsFmt += ", "
			}
			report.EndpointsFmt += "\"" + e + "\""
		}
		report.StartDateFmt = gg.Formatter.FormatDate(report.StartDate, "yyyy-MM-dd HH:mm")
		report.EndDateFmt = gg.Formatter.FormatDate(report.EndDate, "yyyy-MM-dd HH:mm")
		report.DurationTime = fmt.Sprintf("%s", report.EndDate.Sub(report.StartDate))

		// global summary
		var tot *LoadZillaReportSummaryTotals
		e := gg.JSON.ReadFromFile(report.summaryFileName, &tot)
		if nil != e {
			tot = new(LoadZillaReportSummaryTotals)
		}
		report.Total = tot
		tot.Check++
		if report.ErrorsCount > 0 {
			tot.CheckWithErrors++
		} else {
			tot.CheckWithNoErrors++
		}
		tot.Errors += report.ErrorsCount
		tot.Length += report.Length
		tot.QualityServicePerc = 1 - float32(tot.Errors)/float32(tot.Length)
		tot.QualityServicePercFmt = fmt.Sprintf("%.2f%%", tot.QualityServicePerc*100)
		// save file for global summary
		_, _ = gg.IO.WriteTextToFile(gg.JSON.Stringify(tot), report.summaryFileName)
	}

	return report
}

func (instance *LoadZillaReportSummary) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LoadZillaReportSummary) Map() map[string]interface{} {
	return gg.Convert.ToMap(instance.String())
}
