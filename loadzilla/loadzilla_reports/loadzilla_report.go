package loadzilla_reports

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"sync"
)

type LoadZillaReport struct {
	name           string
	root           string
	dataFilename   string
	mux            sync.Mutex
	jobLogger      *gg_log.Logger
	scheduler      *gg_scheduler.Scheduler
	jobSettings    *loadzilla_commons.LoadZillaJobSettings
	reportSettings *loadzilla_commons.LoadZillaJobReportSection
	reportTemplate *LoadZillaReportTemplate
	csvOptions     *gg_utils.CsvOptions
}

func NewLoadZillaReport(jobName string, settings *loadzilla_commons.LoadZillaJobSettings, logger *gg_log.Logger, postman *loadzilla_commons.LoadZillaPostman) *LoadZillaReport {
	instance := new(LoadZillaReport)
	instance.jobSettings = settings
	instance.jobLogger = logger
	instance.reportSettings = instance.jobSettings.Report
	instance.name = jobName
	if len(instance.reportSettings.Name) > 0 {
		instance.name = jobName
	}
	instance.root = gg.Paths.WorkspacePath("reports")
	instance.dataFilename = gg.Paths.Concat(instance.root, instance.name, "data.csv")
	_ = gg.IO.Remove(instance.dataFilename)

	instance.scheduler = gg_scheduler.NewScheduler()
	instance.reportTemplate = NewLoadZillaReportTemplate(logger, postman, instance.reportSettings, instance.dataFilename)
	instance.csvOptions = &gg_utils.CsvOptions{
		Comma:          ",",
		Comment:        "#",
		FirstRowHeader: true,
	}

	instance.init()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaReport) Start() {
	if nil != instance.scheduler {
		instance.scheduler.Start()
	}
}

func (instance *LoadZillaReport) Stop() {
	if nil != instance.scheduler {
		instance.scheduler.Stop()
	}
}

func (instance *LoadZillaReport) Append(index int, name string, uid string, source string, sms string, email string, err error, ms int, i int) error {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	row := NewLoadZillaReportRow(index, name, uid, source, sms, email, err, ms, i)
	return instance.appendRow(row)
}

func (instance *LoadZillaReport) GenerateReport() (string, error) {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	summary := instance.getReportSummary()
	if summary.Length > 0 {
		_, _, err := instance.reportTemplate.MergeReport(summary)
		if nil != err {
			return "", err
		}
		return instance.reportTemplate.FileName(), nil
	}
	return "", nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaReport) init() {
	// ensure dir exists
	_ = gg.Paths.Mkdir(instance.dataFilename)
	// ensure file exists
	if b, _ := gg.Paths.Exists(instance.dataFilename); !b {
		_, _ = gg.IO.WriteTextToFile("", instance.dataFilename)
	}
	// enable report scheduling
	if nil != instance.reportSettings && nil != instance.reportSettings.Schedule && len(instance.reportSettings.Schedule.Timeline) > 0 {
		schedule := instance.reportSettings.Schedule
		instance.scheduler.AddSchedule(&gg_scheduler.Schedule{
			Uid:       schedule.Uid,
			StartAt:   schedule.StartAt,
			Timeline:  schedule.Timeline,
			Payload:   schedule.Payload,
			Arguments: schedule.Arguments,
		})
		instance.scheduler.OnSchedule(func(schedule *gg_scheduler.SchedulerTask) {
			instance.scheduler.Pause()
			defer instance.scheduler.Resume()
			instance.sendReport(instance.reportSettings.AutoRemove)
		})
	}
}

func (instance *LoadZillaReport) read() ([]map[string]string, error) {
	text, err := gg.IO.ReadTextFromFile(instance.dataFilename)
	if nil != err {
		// remove the file because may be damaged
		_ = gg.IO.Remove(instance.dataFilename)
	}
	return gg.CSV.ReadAll(text, instance.csvOptions)
}

func (instance *LoadZillaReport) readRows() ([]*LoadZillaReportRow, error) {
	response := make([]*LoadZillaReportRow, 0)
	data, err := instance.read()
	if nil == err {
		for _, m := range data {
			response = append(response, NewLoadZillaReportRowFromMap(m))
		}
	}
	return response, err
}

func (instance *LoadZillaReport) appendRow(row *LoadZillaReportRow) error {
	return gg.CSV.AppendFile([]map[string]interface{}{row.Map()}, instance.csvOptions, instance.dataFilename)
}

func (instance *LoadZillaReport) getReportSummary() *LoadZillaReportSummary {
	rows, err := instance.readRows()
	if nil != err {
		return new(LoadZillaReportSummary)
	}
	name := instance.name
	return NewLoadZillaReportSummary(instance.root, instance.jobSettings, name, rows)
}

func (instance *LoadZillaReport) sendReport(delete bool) {
	summary := instance.getReportSummary()
	if summary.Length > 0 {
		_ = instance.reportTemplate.SendReport(summary, func(err error) {
			// DELETE
			if delete {
				_ = gg.IO.Remove(instance.dataFilename)
			}
		})
	}
}
