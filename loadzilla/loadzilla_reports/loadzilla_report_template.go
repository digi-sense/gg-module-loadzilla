package loadzilla_reports

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_log"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	_ "embed"
)

//go:embed template.html
var HtmlTemplate string

type LoadZillaReportTemplate struct {
	logger         *gg_log.Logger
	postman        *loadzilla_commons.LoadZillaPostman
	reportFilename string
	dataFilename   string
	settings       *loadzilla_commons.LoadZillaJobReportSection
}

func NewLoadZillaReportTemplate(logger *gg_log.Logger, postman *loadzilla_commons.LoadZillaPostman,
	settings *loadzilla_commons.LoadZillaJobReportSection, dataFilename string) *LoadZillaReportTemplate {
	// create instance
	instance := new(LoadZillaReportTemplate)
	instance.logger = logger
	instance.postman = postman
	instance.settings = settings
	instance.dataFilename = dataFilename
	instance.reportFilename = gg.Paths.Concat(gg.Paths.Dir(dataFilename), "report.html")

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaReportTemplate) FileName() string {
	return instance.reportFilename
}

func (instance *LoadZillaReportTemplate) MergeReport(data *LoadZillaReportSummary) (string, map[string]interface{}, error) {
	model := data.Map()
	// merge
	html, err := instance.merge(model)
	return html, model, err
}

func (instance *LoadZillaReportTemplate) SendReport(data *LoadZillaReportSummary, callback func(err error)) error {
	if nil == instance.postman {
		return nil
	}

	// merge
	html, model, err := instance.MergeReport(data)
	if nil != err {
		return err
	}

	// send
	if nil != instance.settings && nil != instance.settings.Sms &&
		len(instance.settings.Sms.To) > 0 {
		// SMS require an url to download or view the file
	}

	if nil != instance.settings && nil != instance.settings.Email &&
		len(instance.settings.Email.To) > 0 {
		email := instance.settings.Email
		attachments := instance.settings.Email.GetAttachments()
		attachments = append(attachments, instance.dataFilename, instance.reportFilename)
		gg.Formatter.MergeTextRecover(email.Subject, model, email.Subject, func(subject string, _ error) {
			_ = instance.postman.SendEmail(email.To, subject, html, attachments, callback)
		})
	}
	return err
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaReportTemplate) merge(data map[string]interface{}) (string, error) {
	// merge
	s, err := gg.Formatter.MergeHtml(HtmlTemplate, data)
	if nil != err {
		return "", err
	}
	_, err = gg.IO.WriteTextToFile(s, instance.reportFilename)
	return s, err
}
