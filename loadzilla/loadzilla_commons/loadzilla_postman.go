package loadzilla_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"errors"
	"fmt"
)

type LoadZillaPostman struct {
	settings *LoadZillaSettings
	logger   *Logger
}

func NewPostman(settings *LoadZillaSettings, logger *Logger) (*LoadZillaPostman, error) {
	if nil != settings {
		instance := new(LoadZillaPostman)
		instance.settings = settings
		instance.logger = logger

		return instance, nil
	}
	return nil, errors.New("missing_settings")
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaPostman) SendEmail(to, subject, message string, attachments []string, callback func(err error)) error {
	if len(to) > 0 {
		sender, err := gg.Email.NewSender(instance.settings.Email.String())
		if nil == err && nil != sender {
			sender.SendAsync(subject, message, []string{to}, instance.settings.Email.From, attachments, callback)
		}
		return err
	}
	return nil
}

func (instance *LoadZillaPostman) NotifyEmail(to, subject, message string, attachments []string, payload map[string]interface{}, isError bool) (response string) {
	if len(to) > 0 && nil != instance.settings && nil != instance.settings.Email && instance.settings.Email.Enabled {
		defer func() {
			if r := recover(); r != nil {
				// recovered from panic
				m := gg.Strings.Format("[panic] SendEmail: '%s'", r)
				instance.logger.Error(m)
			}
		}()
		sender, err := gg.Email.NewSender(instance.settings.Email.String())
		if nil == err && nil != sender {
			mm, _ := gg.Formatter.Merge(message, payload)
			ms, _ := gg.Formatter.Merge(subject, payload)
			if len(mm) > 0 {
				sender.SendAsync(ms, mm, []string{to}, instance.settings.Email.From, attachments, func(err error) {
					if nil != err {
						instance.logger.Error("loadzilla_postman.NotifyEmail()", err)
					} else {
						instance.logger.Info(fmt.Sprintf("Sent email to '%s' with subject '%s'", to, ms))
					}
				})
				response = mm
			} else {
				sender.SendAsync(subject, message, []string{to}, instance.settings.Email.From, attachments, func(err error) {
					if nil != err {
						instance.logger.Error("loadzilla_postman.NotifyEmail()", err)
					} else {
						instance.logger.Info(fmt.Sprintf("Sent email to '%s' with subject '%s'", to, subject))
					}
				})
				response = message
			}
		}
	}
	return
}

func (instance *LoadZillaPostman) NotifySMS(to, message string, payload map[string]interface{}, isError bool) (response string) {
	if len(to) > 0 && nil != instance.settings && nil != instance.settings.SMS && instance.settings.SMS.Enabled {
		defer func() {
			if r := recover(); r != nil {
				// recovered from panic
				m := gg.Strings.Format("[panic] SendSMS: '%s'", r)
				instance.logger.Error(m)
			}
		}()
		sender := ggx.SMS.NewEngine(instance.settings.SMS.String())
		if nil != sender {
			from := instance.settings.SMS.Provider().Param("from")
			mm, _ := gg.Formatter.Merge(message, payload)
			var err error
			if len(mm) > 0 {
				_, err = sender.SendMessage("default", mm, to, from)
				response = mm
			} else {
				_, err = sender.SendMessage("default", message, to, from)
				response = message
			}
			if nil != err {
				instance.logger.Error(fmt.Sprintf("SendSMS error: '%s'", err))
			}
		}
	}
	return
}
