package loadzilla_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	_ "embed"
)

//go:embed default_job.json
var DefaultJob string

type LoadZillaJobSettings struct {
	Uid      string                     `json:"uid"`
	Enabled  bool                       `json:"enabled"`
	Schedule *LoadZillaJobSchedule      `json:"schedule"`
	Threads  int                        `json:"threads"`
	Action   *LoadZillaJobAction        `json:"action"`
	Report   *LoadZillaJobReportSection `json:"report"`
}

type LoadZillaJobSchedule struct {
	gg_scheduler.Schedule
}

type LoadZillaJobAction struct {
	Method       string                           `json:"method"` // POST, GET, PUT, ...
	Endpoint     string                           `json:"endpoint"`
	Header       map[string]interface{}           `json:"header"`
	Body         interface{}                      `json:"body"`
	TimeoutMs    int                              `json:"timeout"`
	Notification *LoadZillaJobNotificationSection `json:"notification"`
	Script       *LoadZillaJobActionScript        `json:"script"` // optional js script to launch
}

func (instance *LoadZillaJobAction) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *LoadZillaJobAction) Clone() *LoadZillaJobAction {
	var clone *LoadZillaJobAction
	_ = gg.JSON.Read(instance.String(), &clone)
	return clone
}

type LoadZillaJobActionScript struct {
	OnBefore string `json:"on_before"`
	OnAfter  string `json:"on_after"`
}

type LoadZillaJobNotificationSection struct {
	Email *LoadZillaJobMailNotification `json:"email"`
	Sms   *LoadZillaJobSMSNotification  `json:"sms"`
}
type LoadZillaJobSMSNotification struct {
	To      string `json:"to"`
	Message string `json:"message"`
}
type LoadZillaJobMailNotification struct {
	LoadZillaJobSMSNotification
	Subject     string   `json:"subject"`
	Attachments []string `json:"attachments"`
}

func (instance *LoadZillaJobMailNotification) GetAttachments() []string {
	response := make([]string, 0)
	for _, a := range instance.Attachments {
		response = append(response, gg.Paths.WorkspacePath(a))
	}
	return response
}

type LoadZillaJobReportSection struct {
	Name       string                        `json:"name"`
	AutoRemove bool                          `json:"auto-remove"` // remove after send
	Schedule   *LoadZillaJobSchedule         `json:"schedule"`
	Email      *LoadZillaJobMailNotification `json:"email"`
	Sms        *LoadZillaJobSMSNotification  `json:"sms"`
}
