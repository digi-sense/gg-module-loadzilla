package loadzilla_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"bitbucket.org/digi-sense/gg-core/gg_email"
	_ "embed"
	"fmt"
)

//go:embed default_settings.json
var DefaultSettings string

type LoadZillaSettings struct {
	SMS   *LoadZillaSettingsSMS
	Email *LoadZillaSettingsEmail
}

type LoadZillaSettingsSMS struct {
	gg_sms_engine.SMSConfiguration
}

type LoadZillaSettingsEmail struct {
	gg_email.SmtpSettings
	Enabled bool `json:"enabled"`
}

func (instance *LoadZillaSettingsEmail) String() string {
	return gg.JSON.Stringify(instance)
}

func NewLoadZillaSettings(mode string) (*LoadZillaSettings, error) {
	instance := new(LoadZillaSettings)
	err := instance.init(mode)

	return instance, err
}

func (instance *LoadZillaSettings) init(mode string) error {
	if len(mode) == 0 {
		mode = "production"
	}
	dir := gg.Paths.WorkspacePath("./")
	filename := gg.Paths.Concat(dir, fmt.Sprintf("settings.%s.json", mode))
	// ensure settings exists
	_ = gg.Paths.Mkdir(filename)
	if b, _ := gg.Paths.Exists(filename); !b {
		_, _ = gg.IO.WriteTextToFile(DefaultSettings, filename)
	}

	// load settings
	return gg.JSON.ReadFromFile(filename, &instance)
}
