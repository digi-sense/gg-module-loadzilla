package loadzilla_jobs

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	_ "embed"
	"errors"
	"fmt"
)

// LoadZillaJobs main controller that run jobs
type LoadZillaJobs struct {
	root    string
	logger  *loadzilla_commons.Logger
	events  *gg_events.Emitter
	postman *loadzilla_commons.LoadZillaPostman

	jobs map[string]*LoadZillaJob
}

func NewLoadZillaJobs(dirWork string, logger *loadzilla_commons.Logger, events *gg_events.Emitter, postman *loadzilla_commons.LoadZillaPostman) (*LoadZillaJobs, error) {
	instance := new(LoadZillaJobs)
	instance.root = gg.Paths.Concat(dirWork, "jobs")
	instance.logger = logger
	instance.events = events
	instance.postman = postman
	instance.jobs = make(map[string]*LoadZillaJob)

	err := instance.init()

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaJobs) Start() error {
	jobErrors := make([]string, 0)
	if len(instance.jobs) > 0 {
		for _, job := range instance.jobs {
			started, err := job.Start()
			if nil != err {
				jobErrors = append(jobErrors, err.Error())
				instance.logger.Error(fmt.Sprintf("Job '%s' starting error: %s", job.name, err.Error()))
			} else {
				if started {
					instance.logger.Info(fmt.Sprintf("Job '%s' started.", job.name))
				}
			}
		}
	}
	if len(jobErrors) > 0 {
		return errors.New(fmt.Sprintf("%v error/s occurred starting jobs: [%s]", len(jobErrors), gg.Strings.Concat(jobErrors)))
	}
	return nil
}

func (instance *LoadZillaJobs) Stop() {
	if len(instance.jobs) > 0 {
		for _, job := range instance.jobs {
			job.Stop()
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaJobs) init() error {
	// create sample job
	sampleFile := gg.Paths.Concat(instance.root, "./sample/job.json")
	err := gg.Paths.Mkdir(sampleFile)
	if nil != err {
		return err
	}
	if b, _ := gg.Paths.Exists(sampleFile); !b {
		_, err = gg.IO.WriteTextToFile(loadzilla_commons.DefaultJob, sampleFile)
		if nil != err {
			return err
		}
	}

	// load jobs
	list, err := gg.Paths.ListFiles(instance.root, "*.json")
	if nil != err {
		return err
	}
	for _, file := range list {
		if text, e := gg.IO.ReadTextFromFile(file); e == nil && len(text) > 0 {
			var s loadzilla_commons.LoadZillaJobSettings
			err = gg.JSON.Read(text, &s)
			if nil == err {
				name := gg.Paths.FileName(gg.Paths.Dir(file), false)
				job := NewLoadZillaJob(name, instance.root, &s, instance.postman)
				if job.IsValid() {
					instance.jobs[job.GetUid()] = job
				}
			} else {
				instance.logger.Error("Error on loadzilla_jobs.init()", err)
			}
		}
	}

	return nil
}
