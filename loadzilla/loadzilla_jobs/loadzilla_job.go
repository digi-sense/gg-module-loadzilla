package loadzilla_jobs

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httputils"
	"bitbucket.org/digi-sense/gg-core-x/gg_log"
	"bitbucket.org/digi-sense/gg-core-x/gg_scripting"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_reports"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_variables"
	"fmt"
	"strings"
	"time"
)

type LoadZillaJobExecutor interface {
	Execute(action *loadzilla_commons.LoadZillaJobAction) (*httputils.ResponseData, error)
}

type LoadZillaJob struct {
	name        string
	jobRoot     string
	jobSettings *loadzilla_commons.LoadZillaJobSettings
	postman     *loadzilla_commons.LoadZillaPostman

	root         string
	jobScheduler *gg_scheduler.Scheduler
	logger       *gg_log.Logger
	report       *loadzilla_reports.LoadZillaReport
	started      bool
	lastError    error
}

func NewLoadZillaJob(name string, jobRoot string, jobSettings *loadzilla_commons.LoadZillaJobSettings, postman *loadzilla_commons.LoadZillaPostman) *LoadZillaJob {
	instance := new(LoadZillaJob)
	instance.name = name
	instance.jobRoot = jobRoot
	instance.jobSettings = jobSettings
	instance.postman = postman

	instance.root = gg.Paths.Concat(instance.jobRoot, name)
	instance.jobScheduler = gg_scheduler.NewScheduler()

	// personal logger
	logFile := gg.Paths.Concat(gg.Paths.WorkspacePath("logging"), instance.jobSettings.Uid+".log")
	_ = gg.IO.Remove(logFile) // reset at init if any
	instance.logger = gg_log.NewLogger()
	instance.logger.SetFileName(logFile)

	if len(instance.jobSettings.Uid) > 0 {
		// initialize report engine
		instance.report = loadzilla_reports.NewLoadZillaReport(name, instance.jobSettings, instance.logger, instance.postman)

		// load schedule settings
		schedule := instance.jobSettings.Schedule
		instance.jobScheduler.AddSchedule(&gg_scheduler.Schedule{
			Uid:       schedule.Uid,
			StartAt:   schedule.StartAt,
			Timeline:  schedule.Timeline,
			Payload:   schedule.Payload,
			Arguments: schedule.Arguments,
		})
		instance.jobScheduler.OnSchedule(func(schedule *gg_scheduler.SchedulerTask) {
			instance.jobScheduler.Pause()
			defer instance.jobScheduler.Resume()
			instance.doJob()
		})
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaJob) IsValid() bool {
	if nil != instance.jobSettings {
		return len(instance.jobSettings.Uid) > 0
	}
	return false
}

func (instance *LoadZillaJob) IsEnabled() bool {
	if nil != instance.jobSettings {
		return instance.jobSettings.Enabled
	}
	return false
}

func (instance *LoadZillaJob) GetName() string {
	if nil != instance.jobSettings {
		return instance.name
	}
	return ""
}

func (instance *LoadZillaJob) GetUid() string {
	if nil != instance.jobSettings {
		return instance.jobSettings.Uid
	}
	return ""
}

func (instance *LoadZillaJob) Start() (bool, error) {
	if instance.IsEnabled() {
		instance.jobScheduler.Start()
		instance.report.Start()
		return true, nil
	}
	return false, nil
}

func (instance *LoadZillaJob) Stop() {
	instance.jobScheduler.Stop()
	instance.report.Stop()
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZillaJob) doJob() {
	count := instance.jobSettings.Threads
	action := instance.jobSettings.Action.Clone()

	if count > 0 {
		if !instance.started {
			instance.started = true
			instance.logger.Info(fmt.Sprintf("STARTED at %s", gg.Dates.FormatDate(time.Now(), "yyyy-MM-dd HH:mm:ss")))
		}
		pool := gg.Async.NewConcurrentPool(50)
		// add threads
		for i := 0; i < count; i++ {
			pool.Run(func() error {
				return instance.execute(i, action)
			})
		}
		err := pool.Wait().ErrorOrNil()
		pool = nil
		if nil != err {
			instance.logger.Error(fmt.Sprintf("Unhandled error: '%s'", err.Error()))
		}
	}
}

func (instance *LoadZillaJob) execute(index int, action *loadzilla_commons.LoadZillaJobAction) error {
	actionContext := make(map[string]interface{})

	// ON-BEFORE
	if nil != action.Script {
		name := action.Script.OnBefore
		executed, elapsed, response, err := instance.runScript(name, actionContext)
		if executed {
			instance.handleResponse(index, name, elapsed, []byte(response), err, action.Notification)
			// update context
			if len(response) > 0 && gg.Regex.IsValidJsonObject(response) {
				actionContext = gg.Convert.ToMap(response)
			}
		}
	}

	// solve variables in Action
	_ = loadzilla_variables.SolveAction(action, actionContext)

	executor := GetExecutor(action)
	if nil != executor {
		watch := gg.StopWatch.New()
		watch.Start()
		response, respErr := executor.Execute(action)
		watch.Stop()

		milliseconds := watch.Milliseconds()
		if nil == respErr && len(response.Header) > 0 {
			if v, b := response.Header["elapsed_ms"]; b {
				milliseconds = int(gg_utils.Convert.ToFloat32(v))
			}
		}
		instance.handleResponse(index, action.Endpoint, milliseconds,
			response.Body, respErr, action.Notification)
	} else {
		instance.logger.Error(fmt.Sprintf("Missing executor for '%s'. This protocol is not supported!", action.Endpoint))
	}

	// ON-AFTER
	if nil != action.Script {
		name := action.Script.OnAfter
		executed, elapsed, response, err := instance.runScript(name, actionContext)
		if executed {
			instance.handleResponse(index, name, elapsed, []byte(response), err, action.Notification)
		}
	}

	return nil
}

func (instance *LoadZillaJob) runScript(name string, context map[string]interface{}) (executed bool, elapsed int, response string, err error) {
	if len(name) > 0 {
		filename := gg.Paths.Concat(instance.root, name)
		if b, _ := gg.Paths.Exists(filename); b {
			if nil == context {
				context = make(map[string]interface{})
			}
			// run script....
			watch := gg.StopWatch.New()
			watch.Start()
			response, err = ggx.Scripting.Run(&gg_scripting.EnvSettings{
				FileName: filename,
				Context: map[string]interface{}{
					"$ctx": context,
				},
				LogReset: true,
			})
			watch.Stop()
			elapsed = watch.Milliseconds()
			executed = true
		}
	}
	return
}

func (instance *LoadZillaJob) handleResponse(index int, source string, elapsedMs int, data []byte, err error, notification *loadzilla_commons.LoadZillaJobNotificationSection) {
	isError := nil != err
	payload := make(map[string]interface{})
	payload["uid"] = instance.GetUid()
	payload["endpoint"] = source
	payload["source"] = source
	payload["err"] = err
	payload["elapsed"] = gg.Formatter.FormatInteger(elapsedMs, "")
	payload["length"] = gg.Formatter.FormatBytes(len(data))

	var sms, email string
	// notify
	if isError {
		if nil == instance.lastError || instance.lastError.Error() != err.Error() {
			instance.lastError = err // set error
			sms, email = instance.notify(notification, payload, isError)
		}
	} else {
		if nil != instance.lastError {
			instance.lastError = nil // remove error
			sms, email = instance.notify(notification, payload, isError)
		}
	}

	// send to report manager
	_ = instance.report.Append(index, instance.name, instance.GetUid(), source, sms, email, err, elapsedMs, len(data))
}

func (instance *LoadZillaJob) notify(notification *loadzilla_commons.LoadZillaJobNotificationSection, payload map[string]interface{}, isError bool) (sms string, email string) {
	if len(payload) > 0 {
		// log
		if isError {
			text := fmt.Sprintf("[%s - elapsed ms: %s - size: %s] %s",
				payload["source"], payload["elapsed"], payload["length"], payload["err"])
			instance.logger.Error(text)
		} else {
			text := fmt.Sprintf("[%s - elapsed ms: %s - size: %s] Service '%s' is working fine.",
				payload["source"], payload["elapsed"], payload["length"], payload["uid"])
			instance.logger.Info(text)
		}
		if nil != notification {
			if nil != notification.Email {
				email = instance.postman.NotifyEmail(notification.Email.To, notification.Email.Subject,
					notification.Email.Message, notification.Email.GetAttachments(), payload, isError)
			}
			if nil != notification.Sms {
				sms = instance.postman.NotifySMS(notification.Sms.To,
					notification.Sms.Message, payload, isError)
			}
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func GetExecutor(action *loadzilla_commons.LoadZillaJobAction) LoadZillaJobExecutor {
	endpoint := strings.ToLower(action.Endpoint)
	if strings.Index(endpoint, "http") == 0 {
		return &LoadZillaExecuteHttp{}
	} else if strings.Index(endpoint, "ping") == 0 {
		return &LoadZillaExecutePing{}
	}
	// unhandled executor
	return nil
}
