package loadzilla_jobs

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpclient"
	"time"
)

func AddAllHeaders(client *httpclient.HttpClient, headers map[string]interface{}) {
	if nil != headers {
		for k, v := range headers {
			client.AddHeader(k, gg.Convert.ToString(v))
		}
	}
}

func AddDefaultHeaders(method string, body interface{}, client *httpclient.HttpClient) {
	AddAllHeaders(client, GetDefaultHeaders(method, body))
}

func GetDefaultHeaders(method string, body interface{}) map[string]interface{} {
	response := make(map[string]interface{})
	response["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36"
	response["Content-Type"] = getContentType(method, body)
	response["Connection"] = "keep-alive"
	response["Date"] = time.Now().Format("2006-01-02 15:04:05 -0700 MST")
	return response
}

func getContentType(method string, body interface{}) string {
	if method != "GET" && nil != body {
		return "application/json"
	}
	return "text/html"
}
