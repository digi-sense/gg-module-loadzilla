package loadzilla_jobs

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpclient"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httputils"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"strings"
	"time"
)

type LoadZillaExecuteHttp struct {
}

func (instance *LoadZillaExecuteHttp) Execute(action *loadzilla_commons.LoadZillaJobAction) (response *httputils.ResponseData, respErr error) {
	method := action.Method
	endpoint := action.Endpoint
	timeout := action.TimeoutMs
	if len(method) > 0 && len(endpoint) > 0 {
		method = strings.ToUpper(method)
		if timeout < 10 {
			timeout = 30 * 1000 // 30 seconds
		}
		client := httpclient.NewHttpClient()
		AddDefaultHeaders(method, action.Body, client)
		AddAllHeaders(client, action.Header)

		switch method {
		case "POST":
			response, respErr = client.PostTimeout(endpoint, action.Body,
				time.Duration(timeout)*time.Millisecond)
		case "PUT":
			response, respErr = client.PutTimeout(endpoint, action.Body,
				time.Duration(timeout)*time.Millisecond)
		case "DELETE":
			response, respErr = client.DeleteTimeout(endpoint, action.Body,
				time.Duration(timeout)*time.Millisecond)
		default:
			response, respErr = client.GetTimeout(endpoint,
				time.Duration(timeout)*time.Millisecond)
		}
	}
	return
}
