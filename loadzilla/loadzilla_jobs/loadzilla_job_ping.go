package loadzilla_jobs

import (
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httputils"
	"bitbucket.org/digi-sense/gg-core/gg_exec/ping"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"fmt"
	"strings"
)

type LoadZillaExecutePing struct {
}

func (instance *LoadZillaExecutePing) Execute(action *loadzilla_commons.LoadZillaJobAction) (response *httputils.ResponseData, respErr error) {
	response = new(httputils.ResponseData)
	endpoint := strings.ReplaceAll(action.Endpoint, "ping://", "")
	if len(endpoint) > 0 {
		response.StatusCode = 200
		session := ping.NewPingExec()
		r, err := session.PingOnce(endpoint)
		if nil != err {
			response.StatusCode = 500
			respErr = err
		} else {
			if len(r.Rows) > 0 {
				row := r.Rows[0]
				response.Body = []byte(row.String())
				response.Header = map[string]string{
					"elapsed_ms":fmt.Sprintf("%v", row.TimeMs),
				}
			}
		}
	}
	return
}
