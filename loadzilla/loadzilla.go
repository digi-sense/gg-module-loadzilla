package loadzilla

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_jobs"
	"fmt"
	"path/filepath"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		A p p l i c a t i o n
// ---------------------------------------------------------------------------------------------------------------------

type LoadZilla struct {
	mode    string
	root    string
	dirWork string

	settings    *loadzilla_commons.LoadZillaSettings
	logger      *loadzilla_commons.Logger
	stopChan    chan bool
	stopMonitor *stopMonitor
	events      *gg_events.Emitter

	postman *loadzilla_commons.LoadZillaPostman
	jobs    *loadzilla_jobs.LoadZillaJobs
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZilla) Start() (err error) {
	instance.stopChan = make(chan bool, 1)
	if nil != instance.stopMonitor {
		instance.stopMonitor.Start()
	}

	if nil != instance.jobs {
		err = instance.jobs.Start()
	}

	return
}

// Stop Try to close gracefully
func (instance *LoadZilla) Stop() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}

	if nil != instance.jobs {
		instance.jobs.Stop()
	}

	time.Sleep(3 * time.Second)
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}
	return
}

// Exit application
func (instance *LoadZilla) Exit() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}

	return
}

func (instance *LoadZilla) Join() {
	if nil != instance.stopChan {
		<-instance.stopChan
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LoadZilla) doStop(_ *gg_events.Event) {
	_ = instance.Exit()
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func LaunchLoadZilla(mode, cmdStop string, args ...interface{}) (instance *LoadZilla, err error) {
	instance = new(LoadZilla)
	instance.mode = mode

	// paths
	instance.dirWork = gg.Paths.GetWorkspace(loadzilla_commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirWork)

	instance.settings, err = loadzilla_commons.NewLoadZillaSettings(mode)
	if nil == err {
		instance.events = gg.Events.NewEmitter(loadzilla_commons.AppName)
		instance.stopMonitor = newStopMonitor([]string{instance.root, instance.dirWork}, cmdStop, instance.events)
		instance.events.On(loadzilla_commons.EventOnDoStop, instance.doStop)

		// logger as first parameter
		l := gg.Arrays.GetAt(args, 0, nil)
		instance.logger = loadzilla_commons.NewLogger(mode, l)

		// POSTMAN
		instance.postman, err = loadzilla_commons.NewPostman(instance.settings, instance.logger)
		if nil == err {
			// JOBS
			instance.jobs, err = loadzilla_jobs.NewLoadZillaJobs(instance.dirWork, instance.logger, instance.events, instance.postman)
			if nil == err {
				// log started
				instance.logger.Info(fmt.Sprintf("%s %s is running.", loadzilla_commons.AppName, loadzilla_commons.AppVersion))
			}
		}
	}

	return
}
