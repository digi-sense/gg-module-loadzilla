package loadzilla_variables

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_fnvars"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
)

var engine *gg_fnvars.FnVarsEngine

func init() {
	engine = gg.FnVars.NewEngine()
	// register custom

}

func SolveText(text string) (string, error) {
	return engine.SolveText(text)
}

func Solve(input interface{}) (interface{}, error) {
	return engine.Solve(input)
}

func SolveAction(action *loadzilla_commons.LoadZillaJobAction, context map[string]interface{}) (err error) {
	// endpoint
	action.Endpoint, err = engine.SolveText(action.Endpoint)
	if nil != err {
		return
	}

	// header
	if len(action.Header) > 0 {
		for k, v := range action.Header {
			value, e := engine.Solve(v, context)
			if nil == e {
				action.Header[k] = value
			}
		}
	}
	if nil != err {
		return
	}

	// body
	body, e := engine.Solve(action.Body)
	if nil == e && nil != body {
		action.Body = body
	}
	return
}
