package _test

import (
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_variables"
	"fmt"
	"testing"
)

func TestVariables(t *testing.T) {
	text := "this is text with a variable: <var>rnd|alpha|6</var>"
	solved, err := loadzilla_variables.SolveText(text)
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
	if text == solved {
		t.Error("Not solved")
		t.FailNow()
	}
	fmt.Println(solved)
}
