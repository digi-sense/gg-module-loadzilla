package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_log"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_reports"
	"fmt"
	"testing"
)

func TestReportMerge(t *testing.T) {
	gg.Paths.SetWorkspacePath("./_loadzilla")
	// personal logger
	logFile := gg.Paths.Concat(gg.Paths.WorkspacePath("logging"), "default.log")
	_ = gg.IO.Remove(logFile) // reset at init if any
	logger := gg_log.NewLogger()
	logger.SetFileName(logFile)
	// job
	jobSettings := &loadzilla_commons.LoadZillaJobSettings{
		Uid:      "default",
		Enabled:  false,
		Schedule: &loadzilla_commons.LoadZillaJobSchedule{},
		Threads:  1,
		Action:   nil,
		Report:   &loadzilla_commons.LoadZillaJobReportSection{
			Name:       "",
			AutoRemove: false,
			Schedule:   &loadzilla_commons.LoadZillaJobSchedule{},
			Email:      nil,
			Sms:        nil,
		},
	}

	report := loadzilla_reports.NewLoadZillaReport("sample", jobSettings, logger, nil)
	filename, err := report.GenerateReport()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(filename)
}
