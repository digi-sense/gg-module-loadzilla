package main

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla"
	"bitbucket.org/digi-sense/gg-module-loadzilla/loadzilla/loadzilla_commons"
	"flag"
	"log"
	"os"
)

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] APPLICATION '%s' ERROR: %s", loadzilla_commons.AppName, r)
			log.Fatalf(message)
		}
	}()

	//-- command flags --//
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirWork := cmdRun.String("dir_work", gg.Paths.Absolute("./_loadzilla"), "Set a particular folder as main workspace")
	mode := cmdRun.String("m", loadzilla_commons.ModeProduction, "Mode allowed: 'debug' or 'production'")
	quit := cmdRun.String("s", "stop", "Quit Command: Write a command (ex: 'stop') to enable stop mode")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		default:
			panic("Command not supported: " + cmd)
		}
	} else {
		panic("Missing command. i.e. 'run'")
	}

	initialize(dirWork, mode)

	app, err := loadzilla.LaunchLoadZilla(*mode, *quit)
	if nil == err {
		err = app.Start()
		if nil != err {
			log.Panicf("Error starting %s: %s", loadzilla_commons.AppName, err.Error())
		} else {
			// app is running
			app.Join()
		}
	} else {
		log.Panicf("Error launching %s: %s", loadzilla_commons.AppName, err.Error())
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func initialize(dirWork *string, mode *string) {
	gg.Paths.GetWorkspace(loadzilla_commons.WpDirWork).SetPath(*dirWork)
}
